// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/MainMenuWidget.h"
#include "Components/Button.h"
#include "SnakeGameInstance.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Structures.h"

DEFINE_LOG_CATEGORY_STATIC(LogMenuWidget, All, All)

void UMainMenuWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (StartGameButton)
	{
		StartGameButton->OnClicked.AddDynamic(this, &UMainMenuWidget::OnStartGame);
		UE_LOG(LogTemp, Warning, TEXT("StartGameButton is valid"));
	}
	if (QuitGameButton)
	{
		QuitGameButton->OnClicked.AddDynamic(this, &UMainMenuWidget::OnQuitGame);
		UE_LOG(LogTemp, Warning, TEXT("QuitGameButton is valid"));
	}
	if (PreferencesButton)
	{
		PreferencesButton->OnClicked.AddDynamic(this, &UMainMenuWidget::OnPreferencesGame);
		UE_LOG(LogTemp, Warning, TEXT("OnPreferencesGame is valid"));
	}

}

void UMainMenuWidget::OnStartGame()
{
	const FName StartUpLevelName = "Level1";
	UGameplayStatics::OpenLevel(this, StartUpLevelName);
	UE_LOG(LogTemp, Warning, TEXT("OnStartGame"));
	const auto Instance = Cast<USnakeGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	Instance->SetGameState(EGameState::InProgress);
}

void UMainMenuWidget::OnQuitGame()
{
	const auto *World = GetWorld();
	TEnumAsByte<EQuitPreference::Type> QuitPreference = EQuitPreference::Quit;
	UKismetSystemLibrary::QuitGame(World, UGameplayStatics::GetPlayerController(World, 0), QuitPreference, true);
}

void UMainMenuWidget::OnPreferencesGame()
{
	const auto Instance = Cast<USnakeGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	Instance->SetGameState(EGameState::Preferences);
}
