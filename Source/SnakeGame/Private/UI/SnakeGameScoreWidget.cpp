// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/SnakeGameScoreWidget.h"
#include "Engine/GameInstance.h"
#include "Management/SnakeGameInstance.h"
#include "Components/TextBlock.h"

DEFINE_LOG_CATEGORY_STATIC(LogScoreWidget, All, All)

void USnakeGameScoreWidget::OnScoreChanged(int NewScore)
{
	if (ScoreTextBox)
	{
		ScoreTextBox->SetText(FText::FromString(FString::FromInt(NewScore)));
		UE_LOG(LogTemp, Warning, TEXT("New widget score %d"), NewScore);
	}

}

void USnakeGameScoreWidget::NativeConstruct()
{
	Super::NativeConstruct();
	
	const auto World = GetWorld();

	if (!World) return;

	const auto Instance = Cast<USnakeGameInstance>(World->GetGameInstance());
	if (!Instance) return;
	Instance->OnScoreChanged.AddUObject(this, &USnakeGameScoreWidget::OnScoreChanged);

	if (ScoreTextBox)
	{
		ScoreTextBox->SetText(FText::FromString("0"));
		UE_LOG(LogTemp, Warning, TEXT("Score Initialised"));
	}
}
