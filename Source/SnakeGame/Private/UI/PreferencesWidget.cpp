// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/PreferencesWidget.h"
#include "Components/CheckBox.h"
#include "Components/ComboBoxString.h"
#include "Components/Slider.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"

void UPreferencesWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (CancelButton)
	{
		CancelButton->OnClicked.AddDynamic(this, &UPreferencesWidget::OnCancelPreferences);
	}
	if (OkButton)
	{
		OkButton->OnClicked.AddDynamic(this, &UPreferencesWidget::OnOkPreferences);
	}
}

void UPreferencesWidget::OnCancelPreferences()
{
}

void UPreferencesWidget::OnOkPreferences()
{
}

