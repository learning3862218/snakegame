// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/PauseWidget.h"
#include "Components/Button.h"
#include "SnakeGameInstance.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY_STATIC(LogMenuWidget, All, All)

void UPauseWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (ContinueButton)
	{
		ContinueButton->OnClicked.AddDynamic(this, &UPauseWidget::OnContinueGame);
	}
	if (MainMenuButton)
	{
		MainMenuButton->OnClicked.AddDynamic(this, &UPauseWidget::OnMainMenuGame);
	}
	if (PreferencesButton)
	{
		PreferencesButton->OnClicked.AddDynamic(this, &UPauseWidget::OnPreferencesGame);
	}

}

void UPauseWidget::OnContinueGame()
{
	const auto World = GetWorld();
	if (!World) return;
	const auto Instance = Cast<USnakeGameInstance>(World->GetGameInstance());
	if (!Instance) return;
	Instance->SetGameState(EGameState::InProgress);

	UGameplayStatics::SetGamePaused(GetWorld(), false);
}

void UPauseWidget::OnPreferencesGame()
{
	UE_LOG(LogTemp, Warning, TEXT("Preferences pressed"));
	const auto Instance = Cast<USnakeGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (Instance)
	{
		Instance->SetGameState(EGameState::Preferences);
	}

}

void UPauseWidget::OnMainMenuGame()
{
	const FName MenuLevelName = "MainMenu";
	UGameplayStatics::OpenLevel(this, MenuLevelName);
}
