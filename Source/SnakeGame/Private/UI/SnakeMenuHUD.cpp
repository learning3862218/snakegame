// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/SnakeMenuHUD.h"
#include "SnakeGameInstance.h"
#include "Blueprint/UserWidget.h"

void ASnakeMenuHUD::BeginPlay()
{
	Super::BeginPlay();
	const auto World = GetWorld();
	if (!World) return;
	const auto Instance = Cast<USnakeGameInstance>(World->GetGameInstance());
	if (!Instance) return;
	Instance->OnGameStateChanged.AddUObject(this, &ASnakeMenuHUD::OnStateChanged);

	if (MenuWidgetClass)
	{
		MenuWidget = CreateWidget<UUserWidget>(GetWorld(), MenuWidgetClass);
		if (MenuWidget)
		{
			MenuWidget->AddToViewport();
		}
	}
}

void ASnakeMenuHUD::OnStateChanged(EGameState State)
{
	if (State == EGameState::Preferences)
	{
		const auto World = GetWorld();
		if (!World) return;
		const auto PreferencesWidget = CreateWidget<UUserWidget>(World, PreferencesWidgetClass);
		if (PreferencesWidget)
		{
			if (MenuWidget)
				MenuWidget->RemoveFromParent();
			PreferencesWidget->AddToViewport();
		}

	}
}
