// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/SnakeMenuGameModeBase.h"
#include "UI/SnakeMenuPlayerController.h"
#include "UI/SnakeGameHUD.h"

ASnakeMenuGameModeBase::ASnakeMenuGameModeBase()
{
	PlayerControllerClass = ASnakeMenuPlayerController::StaticClass();
	HUDClass = ASnakeGameHUD::StaticClass();
}
