// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/SnakeGameHUD.h"
#include "SnakeGameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "Blueprint/UserWidget.h"

void ASnakeGameHUD::DrawHUD()
{
	Super::DrawHUD();
	
}

void ASnakeGameHUD::BeginPlay()
{
	Super::BeginPlay();

	const auto World = GetWorld();
	if (!World) return;
	const auto Instance = Cast<USnakeGameInstance>(World->GetGameInstance());
	if (!Instance) return;
	Instance->OnGameStateChanged.AddUObject(this, &ASnakeGameHUD::OnStateChanged);


	const auto ScoreHUDWidget = CreateWidget<UUserWidget>(GetWorld(), ScoreHUDWidgetClass);
	if (ScoreHUDWidget)
	{
		ScoreHUDWidget->AddToViewport();
	}
}

void ASnakeGameHUD::OnStateChanged(EGameState State)
{
	if (State == EGameState::Paused)
	{
		const auto World = GetWorld();
		if (!World) return;
		World->GetFirstPlayerController()->SetInputMode(FInputModeUIOnly());
		World->GetFirstPlayerController()->bShowMouseCursor = true;
		UGameplayStatics::SetGamePaused(GetWorld(), true);

		PauseWidget = CreateWidget<UUserWidget>(GetWorld(), PauseWidgetClass);
		if (PauseWidget)
		{
			PauseWidget->AddToViewport();
		}
	}
	else if (State == EGameState::InProgress)
	{
		if (PauseWidget)
		{
			PauseWidget->RemoveFromParent();
		}
		const auto World = GetWorld();
		if (!World) return;
		World->GetFirstPlayerController()->SetInputMode(FInputModeGameOnly());
		World->GetFirstPlayerController()->bShowMouseCursor = false;
	}
	else if(State == EGameState::Preferences)
	{
		if (PauseWidget)
		{
			PauseWidget->RemoveFromParent();
		}
		if (PreferencesWidgetClass)
		{
			const auto PreferencesWidget = CreateWidget<UUserWidget>(GetWorld(), PreferencesWidgetClass);
			PreferencesWidget->AddToViewport();
		}

	}
}
