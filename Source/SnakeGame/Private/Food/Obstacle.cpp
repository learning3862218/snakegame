// Fill out your copyright notice in the Description page of Project Settings.


#include "Obstacle.h"
#include "SnakeBase.h"
#include "SpawnerBase.h"
#include "PlayerPawnBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AObstacle::AObstacle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AObstacle::BeginPlay()
{
	Snake = Cast<APlayerPawnBase>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0))->SnakeActor;
	Super::BeginPlay();

}

// Called every frame
void AObstacle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	LifeTime -= DeltaTime;
	if (LifeTime < 0.f)
	{
		if (Snake)
		{
			Snake->Spawner->DecreaseCurrentObjectsCount();
		}
		this->Destroy();
	}
}

void AObstacle::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto SnakeActor = Cast<ASnakeBase>(Interactor);
		if (IsValid(SnakeActor))
		{
			SnakeActor->KillSnake();
		}
	}

}

