// Fill out your copyright notice in the Description page of Project Settings.


#include "Decrease.h"
#include "SnakeBase.h"
#include "SpawnerBase.h"

// Sets default values
ADecrease::ADecrease()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ADecrease::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ADecrease::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADecrease::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			if (Snake->SnakeElements.Num() <= 2)
			{
				Snake->KillSnake();
			}
			else
			{
				Snake->IsDecrease = true;
			}
		}
		Snake->Spawner->DecreaseCurrentObjectsCount();
		AddPoints();
		this->Destroy();
	}
}

