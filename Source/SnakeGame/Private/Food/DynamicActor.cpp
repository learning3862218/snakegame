#include "DynamicActor.h"
#include "SnakeGameInstance.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ADynamicActor::ADynamicActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	RootComponent = MeshComponent;
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

void ADynamicActor::AddPoints() const
{
	auto Instance = Cast<USnakeGameInstance>(GetGameInstance());
	if (!Instance) return;
	Instance->AddScorePoints(Points);
}

// Called when the game starts or when spawned
void ADynamicActor::BeginPlay()
{
	Super::BeginPlay();
	
}


// Called every frame
void ADynamicActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


