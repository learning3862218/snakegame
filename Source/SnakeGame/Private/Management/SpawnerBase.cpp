// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnerBase.h"
#include "SnakeGameInstance.h"
#include "SnakeBase.h"
#include "Food.h"
#include "Decrease.h"
#include "ZoomIn.h"
#include "ZoomOut.h"
#include "Obstacle.h"
#include "Kismet/KismetMathLibrary.h"
#include "CollisionQueryParams.h"
#include "DrawDebugHelpers.h"

// Sets default values
ASpawnerBase::ASpawnerBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

ASpawnerBase::~ASpawnerBase()
{
	SnakePtr = nullptr;
}

void ASpawnerBase::Update()
{
	int FieldElementsCount = Width * Height;
	int SnakeElementsCount = 0;
	if (SnakePtr)
	{
		SnakeElementsCount = SnakePtr->SnakeElements.Num();
	}
	int FreeElementsCount = FieldElementsCount - SnakeElementsCount;
	if (FreeElementsCount * 0.05f > 20)
	{
		MaxObjectsCount = FreeElementsCount * 0.05f;
	}
	if (CurrentObjectsCount < MaxObjectsCount)
	{
		FVector Location = FVector::ZeroVector;
		FTransform NewTransform;
		Location.Z = 0.f;
		Location.X = UKismetMathLibrary::RandomIntegerInRange(0, Width) * Step - StaticCast<int>(Width / 2) * Step;
		Location.Y = UKismetMathLibrary::RandomIntegerInRange(0, Height) * Step - StaticCast<int>(Height / 2) * Step;
		NewTransform.SetLocation(Location);

		if (TraceObstacles(Location)) return;

		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::DontSpawnIfColliding;
		int SpawnPercent = UKismetMathLibrary::RandomIntegerInRange(0, 100);
		AActor* SpawnedObject = nullptr;
		// ��� - 50%
		if (SpawnPercent < 50)
		{
			SpawnedObject = GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform, SpawnParameters);
		}
		// ���������� - 10%
		else if(SpawnPercent < 60)
		{
			SpawnedObject = GetWorld()->SpawnActor<ADecrease>(DecreaseClass, NewTransform, SpawnParameters);
		}
		// ������  - 15%
		else if (SpawnPercent < 75)
		{
			SpawnedObject = GetWorld()->SpawnActor<AZoomOut>(ZoomOutClass, NewTransform, SpawnParameters);
		}
		// ����� - 15%
		else if (SpawnPercent < 90)
		{
			SpawnedObject = GetWorld()->SpawnActor<AZoomIn>(ZoomInClass, NewTransform, SpawnParameters);
		}
		// ����������� - 10%
		else
		{
			SpawnedObject = GetWorld()->SpawnActor<AObstacle>(ObstacleClass, NewTransform, SpawnParameters);
		}
		if (IsValid(SpawnedObject))
		{
			IncreaseCurrentObjectsCount();
		}
	}
}

void ASpawnerBase::DecreaseCurrentObjectsCount()
{
	--CurrentObjectsCount;
}

void ASpawnerBase::IncreaseCurrentObjectsCount()
{
	++CurrentObjectsCount;
}

void ASpawnerBase::SetSnakePtr(ASnakeBase* Ptr)
{
		SnakePtr = Ptr;
}

bool ASpawnerBase::TraceObstacles(FVector& TraceLocation)
{
	if (!World) return true;

	FHitResult RV_Hit(ForceInit);

	FVector LocationStart = TraceLocation;
	LocationStart.Z = LocationStart.Z + 10.0f;
	FVector LocationEnd = LocationStart;
	LocationEnd.X += Step;
	FVector Rotator = LocationEnd - LocationStart;
	FVector Axis(0.0f, 0.0f, 1.0f);

	World->LineTraceSingleByObjectType(RV_Hit, LocationStart + Rotator, LocationStart, ObjectQueryParams, RV_TraceParams);
	DrawDebugLine(World, LocationStart + Rotator, LocationStart, FColor::Red, false, 5.0f, 0, 2.0f);
	if (RV_Hit.bBlockingHit) return true;
	for (int i = 0; i < 8; ++i)
	{
		World->LineTraceSingleByObjectType(RV_Hit, LocationStart, LocationStart + Rotator, ObjectQueryParams, RV_TraceParams);
		DrawDebugLine(World, LocationStart, LocationStart + Rotator, FColor::Red, false, 5.0f, 0, 2.0f);
		if (RV_Hit.bBlockingHit) return true;
		Rotator = Rotator.RotateAngleAxis(45.0f, Axis);
	}
	return false;
}

// Called when the game starts or when spawned
void ASpawnerBase::BeginPlay()
{
	Super::BeginPlay();

	World = GetWorld();
	if (IsValid(World))
	{
		GameInstance = Cast<USnakeGameInstance>(World->GetGameInstance());
		if (IsValid(GameInstance))
		{
			Width = GameInstance->FieldWidth;
			Height = GameInstance->FieldHeight;
			Step = GameInstance->FieldElementSize;
			XOffset = (Width * Step) / 2.0;
			YOffset = (Height * Step) / 2.0;
		}
		RV_TraceParams = FCollisionQueryParams(FName(TEXT("RV_Trace")), true, this);
		RV_TraceParams.bTraceComplex = true;
		RV_TraceParams.bReturnPhysicalMaterial = false;
		ObjectQueryParams.AddObjectTypesToQuery(ECC_WorldDynamic);
		ObjectQueryParams.AddObjectTypesToQuery(ECC_GameTraceChannel1);
		ObjectQueryParams.AddObjectTypesToQuery(ECC_GameTraceChannel2);
		ObjectQueryParams.AddObjectTypesToQuery(ECC_GameTraceChannel3);
	}
}

// Called every frame
void ASpawnerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

