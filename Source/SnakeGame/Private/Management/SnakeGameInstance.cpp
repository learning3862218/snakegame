// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeGameInstance.h"
#include "Engine.h"

void USnakeGameInstance::Init()
{
	Super::Init();
	GEngine->GameUserSettings->SetVSyncEnabled(true);
	GEngine->GameUserSettings->ApplySettings(true);
	GEngine->GameUserSettings->SaveSettings();
	GEngine->Exec(GetWorld(), TEXT("t,MaxFPS 60"));
	
}

int USnakeGameInstance::GetScorePoints()
{
	return CurrentPoints;
}

void USnakeGameInstance::SetGameState(EGameState State)
{
	if (GameState == State) return;
	GameState = State;
	OnGameStateChanged.Broadcast(GameState);
}

void USnakeGameInstance::AddScorePoints(int PointsAdded)
{
	CurrentPoints += PointsAdded;
	UE_LOG(LogTemp, Warning, TEXT("New score %d"), CurrentPoints);
	OnScoreChanged.Broadcast(CurrentPoints);
}
