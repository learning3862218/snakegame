// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElementBase.h"
#include "PlayerPawnBase.h"
#include "Components/StaticMeshComponent.h"
#include "SnakeBase.h"
#include "Kismet/GameplayStatics.h"
#include "SnakeGameInstance.h"

// Sets default values
ASnakeElementBase::ASnakeElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	RootComponent = MeshComponent;
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void ASnakeElementBase::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ASnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeElementBase::SetFirstElementType_Implementation()
{
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
}

void ASnakeElementBase::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{	
		auto SnakeElement = Cast<ASnakeElementBase>(Interactor);
		if (Snake->SnakeElements[1] != this)
		{
			Snake->KillSnake();
		}
	}
}

void ASnakeElementBase::HandleBeginOverlap
	(UPrimitiveComponent* OverlapedComponent, 
	AActor* OtherActor, 
	UPrimitiveComponent* OtherComp, 
	int32 OtherBodyIndex, 
	bool bFromSweep, 
	const FHitResult& SweepResult)
{
	if (IsValid(SnakeOwner))
	{
		SnakeOwner->SnakeElementOverlap(this, OtherActor);
	}
}

void ASnakeElementBase::DeleteSnakeElement()
{
	if (this)
	{
		this->Destroy();
	}
}

void ASnakeElementBase::SetInterval(float NewInterval)
{
	Interval = NewInterval;
}

void ASnakeElementBase::SetMovementDirection(EMovementDirection MovDirection)
{
	MovementDirection = MovDirection;
	UpdateMovement();
}

EMovementDirection ASnakeElementBase::GetMovementDirection()
{
	return MovementDirection;
}

void ASnakeElementBase::UpdateMovement()
{
	DeltaMovement = FVector::ZeroVector;
	switch (MovementDirection)
	{
	case EMovementDirection::UP:
		DeltaMovement.X = Interval;
		break;
	case EMovementDirection::DOWN:
		DeltaMovement.X = Interval * -1.f;
		break;
	case EMovementDirection::LEFT:
		DeltaMovement.Y = Interval;
		break;
	case EMovementDirection::RIGHT:
		DeltaMovement.Y = Interval * -1.f;
		break;
	}

}

FVector ASnakeElementBase::GetDeltaMovement()
{
	return DeltaMovement;
}
