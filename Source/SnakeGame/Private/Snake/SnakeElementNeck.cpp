// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElementNeck.h"
#include "SnakeBase.h"

void ASnakeElementNeck::BeginPlay()
{
	Super::BeginPlay();
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementNeck::HandleBeginOverlap);
}

ASnakeElementNeck::ASnakeElementNeck()
{
	PrimaryActorTick.bCanEverTick = false;
}

void ASnakeElementNeck::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		auto SnakeElement = Cast<ASnakeElementBase>(Interactor);
		if (Snake->SnakeElements[1] != this)
		{
			Snake->KillSnake();
		}
	}
}