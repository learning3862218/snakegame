// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Structures.h"
#include "SnakeGameInstance.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FOnScoreChanged, int);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnGameStateChangedSignature, EGameState);

UCLASS()
class SNAKEGAME_API USnakeGameInstance : public UGameInstance
{
	GENERATED_BODY()

	virtual void Init() override;
	
public:

	FOnGameStateChangedSignature OnGameStateChanged;
	FOnScoreChanged OnScoreChanged;

	UPROPERTY(EditDefaultsOnly, Category = "Field parameters")
	int FieldHeight = 10;
	UPROPERTY(EditDefaultsOnly, Category = "Field parameters")
	int FieldWidth = 10;
	UPROPERTY(EditDefaultsOnly, Category = "Field parameters")
	float FieldElementSize = 100.f;
	UPROPERTY(EditDefaultsOnly, meta = (ClampMin = 0.5f, ClampMax = 10.0f), Category = "Field parameters")
	float Speed = 2.f;
	UPROPERTY()
	int CurrentPoints = 0;


	UFUNCTION()
	void AddScorePoints(int PointsAdded);

	UFUNCTION()
	int GetScorePoints();

	void SetGameState(EGameState State);

private:
	EGameState GameState = EGameState::ReadyToStart;

};
