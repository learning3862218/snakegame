// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

UENUM(BlueprintType)
enum class EGameState : uint8
{
	ReadyToStart = 0,
	InProgress,
	Paused,
	Preferences,
	WinLevel,
	LoseLevel,
	GameOver
};

DECLARE_MULTICAST_DELEGATE_OneParam(FOnGameStateChangedSignature, EGameState);
