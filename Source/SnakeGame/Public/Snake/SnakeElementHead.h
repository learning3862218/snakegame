// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "SnakeElementHead.generated.h"

class APlayerPawnBase;
class UWorld;
class USnakeGameInstance;
enum class EMovementDirection;


UCLASS()
class SNAKEGAME_API ASnakeElementHead : public ASnakeElementBase
{
	GENERATED_BODY()

private:

	FVector MoveToLocation;
	FVector MoveFromLocation;
	UWorld* World;
	USnakeGameInstance* GameInstance;
	APlayerPawnBase* Pawn;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

public:
	ASnakeElementHead();
	virtual void Interact(AActor* Interactor, bool bIsHead) override;

};
