// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeMenuGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API ASnakeMenuGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASnakeMenuGameModeBase();
	
};
