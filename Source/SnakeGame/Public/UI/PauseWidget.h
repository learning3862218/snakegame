// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PauseWidget.generated.h"

class UButton;

UCLASS()
class SNAKEGAME_API UPauseWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(meta = (BindWidget))
	UButton* ContinueButton;

	UPROPERTY(meta = (BindWidget))
	UButton* MainMenuButton;

	UPROPERTY(meta = (BindWidget))
	UButton* PreferencesButton;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UUserWidget> PreferencesWidgetClass;

	// Called when the game starts or when spawned
	virtual void NativeOnInitialized() override;

private:
	UFUNCTION()
	void OnContinueGame();

	UFUNCTION()
	void OnPreferencesGame();

	UFUNCTION()
	void OnMainMenuGame();

};
