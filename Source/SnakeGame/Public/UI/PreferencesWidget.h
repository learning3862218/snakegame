// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PreferencesWidget.generated.h"

class UButton;
class USlider;
class UCheckBox;
class UComboBoxString;

UCLASS()
class SNAKEGAME_API UPreferencesWidget : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(meta = (BindWidget))
	UButton* CancelButton;

	UPROPERTY(meta = (BindWidget))
	UButton* OkButton;

	UPROPERTY(meta = (BindWidget))
	USlider* MusicVolumeSlider;

	UPROPERTY(meta = (BindWidget))
	USlider* EffectsVolumeSlider;

	UPROPERTY(meta = (BindWidget))
	UCheckBox* WindowedCheckBox;

	UPROPERTY(meta = (BindWidget))
	UComboBoxString* ResolutionComboBoxString;

	// Called when the game starts or when spawned
	virtual void NativeOnInitialized() override;

private:
	UFUNCTION()
	void OnCancelPreferences();

	UFUNCTION()
	void OnOkPreferences();

};
