// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Structures.h"
#include "SnakeGameHUD.generated.h"

class UUserWidget;

UCLASS()
class SNAKEGAME_API ASnakeGameHUD : public AHUD
{
	GENERATED_BODY()

public:

	virtual void DrawHUD() override;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UUserWidget> ScoreHUDWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UUserWidget> PauseWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UUserWidget> PreferencesWidgetClass;

	virtual void BeginPlay() override;


private:
	void OnStateChanged(EGameState State);
	UUserWidget *PauseWidget = nullptr;
};
