// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SnakeGameScoreWidget.generated.h"

/**
 * 
 */
class USnakeGameInstance;
//class UTextBlock;

UCLASS()
class SNAKEGAME_API USnakeGameScoreWidget : public UUserWidget
{
	GENERATED_BODY()
private:

public:
	void OnScoreChanged(int NewScore);

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UTextBlock* ScoreTextBox;

protected:
	// Called when the game starts or when spawned
	virtual void NativeConstruct()override;

	UPROPERTY(EditDefaultsOnly)
	int32 FontSize = 32;
	
};
