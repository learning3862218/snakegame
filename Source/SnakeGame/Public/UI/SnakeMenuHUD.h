// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Structures.h"
#include "SnakeMenuHUD.generated.h"

/**
 * 
 */


UCLASS()
class SNAKEGAME_API ASnakeMenuHUD : public AHUD
{
	GENERATED_BODY()

protected:
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UUserWidget> MenuWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UUserWidget> PreferencesWidgetClass;

	virtual void BeginPlay() override;

private:
	void OnStateChanged(EGameState State);
	UUserWidget* MenuWidget = nullptr;
};
