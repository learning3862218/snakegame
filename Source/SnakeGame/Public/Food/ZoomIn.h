// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "DynamicActor.h"
#include "ZoomIn.generated.h"

class APlayerPawnBase;
class ASnakeBase;

UCLASS()
class SNAKEGAME_API AZoomIn : public ADynamicActor, public IInteractable
{
	GENERATED_BODY()

private:
	UPROPERTY(EditDefaultsOnly)
	float ZoomStep = 0.02;
	bool Zoomed = false;
	APlayerPawnBase* PlayerPawn;
	float NewCameraLocationZ;
	ASnakeBase* Snake;

public:	
	// Sets default values for this actor's properties
	AZoomIn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	virtual void Interact(AActor* Interactor, bool bIsHead) override;

};
