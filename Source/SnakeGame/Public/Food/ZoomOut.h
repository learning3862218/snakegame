// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "DynamicActor.h"
#include "ZoomOut.generated.h"

class APlayerPawnBase;
class ASnakeBase;

UCLASS()
class SNAKEGAME_API AZoomOut : public ADynamicActor, public IInteractable
{
	GENERATED_BODY()

private:
	UPROPERTY(EditDefaultsOnly)
	float ZoomStep = 0.02;
	bool Zoomed = false;
	APlayerPawnBase* PlayerPawn;
	float NewCameraLocationZ;
	ASnakeBase* Snake;

public:	
	// Sets default values for this actor's properties
	AZoomOut();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	virtual void Interact(AActor* Interactor, bool bIsHead) override;

};
